import React from "react";
import ReactDOM from "react-dom";
import AppContainer from './AppContainer/AppContainer';

const Index = () => {
  return <AppContainer />;
};

ReactDOM.render(<Index />, document.getElementById("index"));