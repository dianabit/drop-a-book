import React from 'react';
import './BodyMiddleArea.scss';
import Button from '../../../Layout/Button/Button'
import Paragraph from '../../../Layout/Paragraph/Paragraph'
import { bannerButtonHide, bannerButtonShow, bannerParagraphs } from '../../../utils/data';

function BodyMiddleArea(props) {
const { text, onButtonClick, isToggled } = props;

return (
  <React.Fragment>  
    <div className="bodyMiddleArea" role="banner">
      <div className="bodyMiddleArea_text">
        <Paragraph text={text} />
      </div>
      <Button text={isToggled ? bannerButtonHide : bannerButtonShow} onClick={onButtonClick} />
    </div>
    {isToggled && (
      <div className="bodyMiddleArea-isToggled">
        <Paragraph text={bannerParagraphs[0]} />
        <Paragraph text={bannerParagraphs[1]} />
      </div>
      )}
  </React.Fragment>
  )
}

export default BodyMiddleArea;