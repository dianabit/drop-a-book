import React from 'react';
import './BodyBottomArea.scss';
import Heading from '../../../Layout/Heading/Heading'
import TextCollection from '../../../Layout/TextCollection/TextCollection';
import Testimonial from '../../../Layout/Testimonial/Testimonial';
import { title } from '../../../utils/data';

function BodyBottomArea(props) {
  const { textCollections, testimonial} = props;
  
  return (
    <div className="bodyBottomArea" id="bodyBottomArea">
      <div className="bodyBottomArea_title">
        <Heading 
          text={title}  
        />
      </div>
      <div className="bodyBottomArea_content">
        <div className="bodyBottomArea_content_testimonial">
          <Testimonial 
            title={testimonial.title}
            content={testimonial.content}
            name={testimonial.name}
            company={testimonial.company}
            role={testimonial.role}
          />
        </div>
        <div className="bodyBottomArea_content_description">
          {textCollections.map(collection => (
            <TextCollection 
              paragraphs={collection.paragraphs}
              title={collection.title}
            />
          ))}
        </div>
      </div>
    </div>
    )
  }

export default BodyBottomArea;