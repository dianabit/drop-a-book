import React from 'react';
import './BodyContainer.scss';
import BodyMiddleArea from './components/BodyMiddleArea/BodyMiddleArea';
import BodyBottomArea from './components/BodyBottomArea/BodyBottomArea';
import LargeLogo from '../Layout/LargeLogo/LargeLogo';
import Link from '../Layout/Link/Link';
import { bannerMainText, textCollections, testimonial } from '../utils/data';

class BodyContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isBannerToggled: false }

    this.toggleVisibility = this.toggleVisibility.bind(this);
  }
  
  toggleVisibility() {
    this.setState(prevState => ({
      isBannerToggled: !prevState.isBannerToggled,
    }));
  }

  render() {
    const { isBannerToggled } = this.state;

    return (
      <main className="bodyContainer" role="main">
        <div className="bodyContainer_top">
          <Link alt="Logo" href='#bodyBottomArea'>
              <LargeLogo />
          </Link>
        </div>
        <BodyMiddleArea
          text={bannerMainText}
          buttonText="More info"
          isToggled={isBannerToggled}
          onButtonClick={this.toggleVisibility}
        />
        <BodyBottomArea
          testimonial={testimonial}
          textCollections={textCollections}
        />
      </main>
    )
  }
}

export default BodyContainer;