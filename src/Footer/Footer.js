import React from 'react';
import FooterSection from './components/FooterSection/FooterSection';
import SmallLogo from '../Layout/SmallLogo/SmallLogo';
import SocialMediaSection from '../Layout/SocialMediaSection/SocialMediaSection';
import {footerSections, socialLinks} from '../utils/data';;
import './Footer.scss';
import Link from '../Layout/Link/Link';

function Footer(props) {
return (
  /* used aria roles to describe the structure of the web page, for accessibility purposes */
  <footer className="footer" data-track-zone="footer" role="contentinfo">
    <Link alt="Logo">
      <div className="footer_logo">
        <SmallLogo />
      </div>
    </Link>
    <div className="footer_sections">
      {footerSections.map((section, k) => (    
        <FooterSection
          elements={footerSections[k].elements}
          title={footerSections[k].title}
        />
      ))}
    </div>
    <SocialMediaSection
      socialLinks={socialLinks}
    />
  </footer>
  )
}

export default Footer;