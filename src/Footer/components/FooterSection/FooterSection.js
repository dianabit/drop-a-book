import React from 'react';
import TextItem from '../../../Layout/TextItem/TextItem';
import './FooterSection.scss';
import Link from '../../../Layout/Link/Link';

function FooterSection(props) {
const { title, elements} = props;
  
return (
  <div className="footerSection">
    <TextItem className="footerSection_title" text={title} />
    <ul className="footerSection_elements">
      {elements.map(el => (  
        <li>
          <Link alt="Footer Item">
            <TextItem text={el} />
          </Link>
        </li>
      ))}
    </ul>
  </div>
  )
}

export default FooterSection;