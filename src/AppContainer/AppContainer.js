import React from 'react';
import './AppContainer.scss';
import Header from '../Header/HeaderContainer';
import Body from '../Body/BodyContainer';
import Footer from '../Footer/Footer';

class AppContainer extends React.Component {

  render() {
    
    return (
    <div className="appContainer">
      <Header />
      <Body />
      <Footer />
    </div>
    )
  }
}

export default AppContainer;