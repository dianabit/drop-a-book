import React from 'react';
import Link from '../../../Layout/Link/Link';
import TextItem from '../../../Layout/TextItem/TextItem';
import './DropdownMenu.scss';
import Button from '../../../Layout/Button/Button';

function DropdownMenu(props) {
  const { isMenuToggled, onClick, navItems } = props;
  
  return (
    <div className="dropdownMenu">
      <Button hasIcon onClick={onClick} iconName="fas fa-bars" text="Menu" />
      {isMenuToggled && (
        <ul className="dropdownMenu_dropdown">
          {navItems.map(item => (
            <li>
              <Link alt="Dropdown Menu Item">
                <TextItem text={item} />
              </Link>
            </li>
          ))}
         </ul>
      )}
    </div>
  )
}

export default DropdownMenu;