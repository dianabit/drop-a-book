import React from 'react';
import Link from '../../../Layout/Link/Link';
import TextItem from '../../../Layout/TextItem/TextItem'
import './Menu.scss';

function Menu(props) {
  const { navItems, ref } = props;
  return (
    /* used aria-label to define a string for each significat element of the web page, for accessibility purposes */
    <nav className="menu" aria-label="Navigation" role="navigation">
      {navItems.map(item => (
        <div className="menu_item" aria-labelledby={item.main}> 
          <Link alt="Menu Item">
            <TextItem id={item.main} text={item.main} />
          </Link>
          <div className="menu_item_dropDown">
            {item.extraItems && item.extraItems.map(extraItem => (
              <Link alt="Sub Menu Item">
                <TextItem id={extraItem} text={extraItem} />
              </Link>
            ))}
          </div>
        </div>
      ))}
    </nav>
  )
}

export default Menu;