import React from 'react';
import Menu from './components/Menu/Menu';
import './HeaderContainer.scss';
import DropdownMenu from './components/DropdownMenu/DropdownMenu';
import { menuItems, dropdownMenuItems } from '../utils/data';

class HeaderContainer extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = { 
      isMenuToggled: false
    }

    this.toggleVisibility = this.toggleVisibility.bind(this);
  }
  /* A function that toggles visibility of the dropdown menu */
  toggleVisibility() {
    this.setState(prevState => ({
      /* Toggling state, changing it to be different from the one in the previous render */
      isMenuToggled: !prevState.isMenuToggled,
    }));
  }

  render() {
    const { isMenuToggled } = this.state;

    return (
    <header className="headerContainer">
      <Menu navItems={menuItems} />
      <DropdownMenu
        isMenuToggled={isMenuToggled}
        onClick={this.toggleVisibility}
        navItems={dropdownMenuItems}
      />
    </header>
    )
  }
}

export default HeaderContainer;