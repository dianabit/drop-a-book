import React from 'react';
import TextItem from '../TextItem/TextItem';
import './SocialMediaSection.scss';
import Link from '../Link/Link';
import { socialHeader } from '../../utils/data';

const linkToIconMap = {
  facebook: 'fab fa-facebook-f',
  twitter: 'fab fa-twitter',
  dribbble: 'fab fa-dribbble',
};

const linkToExternalMap = {
  facebook: 'http://www.facebook.com',
  twitter: 'http://www.twitter.com',
  dribbble: 'http://www.dribbble.com',
};

/* a function to return the social-media fontawesome class based on their names */
function getClassName(link) {
  if (Object.keys(linkToIconMap).indexOf(link) !== -1) {
    return linkToIconMap[link];
  }
  return link;
}

/* a function to return the social-media links based on their names */
function getExternalLink(link) {
  if (Object.keys(linkToExternalMap).indexOf(link) !== -1) {
    return linkToExternalMap[link];
  }
  return link;
}

function SocialMediaSection(props) {
const { socialLinks} = props;
  
return (
  <div className="socialMediaSection">
    <TextItem className="socialMediaSection_text" text={socialHeader} />
    <ul className="socialMediaSection_links">
      {socialLinks.map((link, k) => (
        <li aria-label={link}>
          <Link href={getExternalLink(link)} alt="Social Media Icon" target="blank">
            <i className={getClassName(link)}/>
          </Link>
        </li>
      ))}
    </ul>
  </div>
  )
}

export default SocialMediaSection;