import React from 'react';
import './SmallLogo.scss';

function SmallLogo() {
  return (
   <div className="smallLogo">
     <i className="fas fa-book-reader smallLogo_icon"></i>
     <div className="smallLogo_text">
      <h3>Drop a book</h3>
     </div>
   </div>
  )
}

export default SmallLogo;