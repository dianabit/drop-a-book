import React from 'react';
import './Link.scss';

function Link(props) {
  const { children, href, onClick, alt, target } = props;
  
  return (
    <a className="link" href={href} alt={alt} target={target} role="button" onCLick={onClick} tabindex="0">
      {children}
    </a>
  )
}

export default Link;