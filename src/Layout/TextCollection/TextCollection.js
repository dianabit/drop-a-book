import React from 'react';
import './TextCollection.scss';
import Subheading from '../Subheading/Subheading';
import Paragraph from '../Paragraph/Paragraph';

function TextCollection(props) {
  const { title, paragraphs } = props;
  
  return (
    <div className="textCollection" role="group">
      <Subheading className="textCollection_subheading" text={title}/>
      <div className="textCollection_paragraph">
        {paragraphs.map((p, k) => (
          <Paragraph key={k} text={p} />
        ))}
      </div>
    </div>
    )
  }

export default TextCollection;