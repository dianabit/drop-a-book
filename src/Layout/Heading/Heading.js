import React from 'react';
import './Heading.scss';

function Heading(props) {
  const { text } = props;
  
  return (
    <h2 className="heading" tabindex="0">{text}</h2>
    )
  }

export default Heading;