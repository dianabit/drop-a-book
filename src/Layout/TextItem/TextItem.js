import React from 'react';

function TextItem(props) {
  const { text, id } = props;
  return (
    <h4 tabindex="0" id={id}>{text}</h4>
  )
}

export default TextItem;