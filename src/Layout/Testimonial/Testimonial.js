import React from 'react';
import './Testimonial.scss';
import Subheading from '../Subheading/Subheading';
import Paragraph from '../Paragraph/Paragraph';

function Testimonial(props) {
  const {title, content, name, company, role} = props;
  return (
    <div className="testimonial">
      <Subheading text={title} />
      <div className="testimonial_content">
        <Paragraph text={content} />
      </div>
      <div className="testimonial_signature">
        <Paragraph text={name} />
        <Paragraph text={company} />
        <Paragraph text={role} />
      </div>
    </div>
  )
}

export default Testimonial;