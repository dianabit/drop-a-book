import React from 'react';
import './Button.scss';
import TextItem from '../TextItem/TextItem';

function Button(props) {
  const {text, onClick, hasIcon, iconName} = props;
  return (
    /* used tabindex to indicatethat the element should be focusable, for accessibility purposes */
    <button type="button" className={!hasIcon ? 'button' : 'button-hasIcon'} onClick={onClick} tabindex="0">
      {hasIcon && (
        <i className={`${iconName} button_icon`} />
      )}
      <div className="button_text">
        <TextItem text={text} />
      </div>
    </button>
  )
}

export default Button;