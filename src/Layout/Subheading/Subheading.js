import React from 'react';
import './Subheading.scss'

function Subheading(props) {
  const {text} = props;
  return (
      <h3 tabindex="0" className="subheading">{text}</h3>
  )
}

export default Subheading;