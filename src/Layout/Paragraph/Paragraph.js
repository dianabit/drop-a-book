import React from 'react';
import './Paragraph.scss';

function Paragraph(props) {
  const {text} = props;
  return (
      <p className="paragraph" tabindex="0">{text}</p>
  )
}

export default Paragraph;