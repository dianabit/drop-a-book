import React from 'react';
import './LargeLogo.scss';

function LargeLogo() {
  return (
   <div className="largeLogo">
     <i className="fas fa-book-reader largeLogo_icon" />
     <div className="largeLogo_text">
      <h1 tabindex="0">Drop a book</h1>
     </div>
   </div>
  )
}

export default LargeLogo;