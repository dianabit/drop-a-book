// A file containing all texts used in the project

export const menuItems = [
  {
    main: 'Product',
    extraItems: ['Donate', 'Receive', 'Delivery']
  }, 
  {
    main: 'About',
    extraItems: ['Our story', 'The Team']
  }, 
  {
    main: 'Portfolio',
    extraItems: ['People', 'Books', 'Projects', 'Campaigns']
  }, 
  {
    main: 'Team',
    extraItems: ['Meet us', 'Support us', 'Join']
  }, 
  {
    main: 'Contact',
    extraItems: ['Get in touch']
  }, 
];

export const dropdownMenuItems = [
  'Support us', 'Media', 'Help'
];

export const bannerMainText = "Together, we can put a book in everyone’s hands. We are connecting people who want to read with the ones who want to help.";

export const bannerParagraphs = [
  "How it works? Very simple! Just contact us and let us know about the book or books you want to share with others. We will then instantly make all arrangements for your books to be taken over and sent to people who need them.",
  "With the help of professionals, we know exactly the needs and preferences of people who have subscribed for receiving books.",
  "The process of donating, from the point of sending to receiving takes from 2 to 8 working weeks, depending on the distance between the countries.",
 ]

export const bannerButtonShow = "More info";

export const bannerButtonHide = "Hide info";

export const title = "Be better and help others be, too. With the help of dedicated people, we can change the world to be a better place.";

export const textCollections = [
  {
    title: 'Changing the world together, book by book...',
    paragraphs: [
     'Yes, that\'s right, because what’s a world without books? Donating them after you have already enjoyed them is a great way to let others be favored and show appreciation.',
     'If you are a reader that has already benefited Drop a Book services, you can show your appreciation by donating them further and therefore helping others.',
    ]
  },
  {
    title: 'Create an impact in someone’s life by giving them the joy of reading and support continuous education.',
    paragraphs: [
      'Imagine a world where everyone has access to education, where everyone is able to learn and be informed about the outer world. This is a great purpose in which Drop a Book represents a step further. Now, you can be part of this great purpose.'
    ]
  }
] 

export const testimonial = {
  title: 'Fantastic service, I\'m excited!',
  content: 'I have had the honour to donate ten books already through Drop a Book and I must say it is an incredible satisfaction to receive much much appreciation from people reading the books. I am glad to help in making the world a better place!',
  name: 'Marta Colin',
  company: 'Citizens Advice',
  role: 'Marketing Executive Officer'
}

export const footerSections = [
  { title: 'Product',
    elements: [
      'Features', 'Promo', 'Download'
    ]
  },
  { title: 'Contact',
    elements: [
      'Find us', 'FAQ', 'Help'
    ]
  },
];

export const socialHeader = "Follow us"

export const socialLinks = [
  'facebook', 'twitter', 'dribbble'
]