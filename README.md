Clone the source locally:
$ git clone https://dianabit@bitbucket.org/dianabit/drop-a-book.git
$ cd drop-a-book

Use your package manager to install npm.
$ sudo apt-get install npm nodejs-legacy

Install project dependencies:
$ npm install

Start the app:
$ npm run start

User interactions:
1. Check compatibility with the given design
2. Go through all elements of the static web page and check their interactivity
3. Test responsive versions
4. Consider testing the accessibility of the web page

Things I would do to improve the project:

1. PERFORMANCE: I would minify the CSS files and try to exclude @import from the CSS files.

2. CSS ANIMATIONS: I would include animations/transitions in the project (dropdown menu, navigation items, more-info button).

3. Go further with the interaction and consider React Routing for a complex Single Page Application through route navigation.